package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	private int ganar;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int paraGanar)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
		ganar=paraGanar;
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO

		int random = (int) (0 + (Math.random() * tablero[1].length));
		boolean exito=registrarJugada(random);
		while ( !exito)
		{
			random = (int) (0 + (Math.random() * tablero[1].length));
			exito=registrarJugada(random);

		}


	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		//TODO

		String pos= tablero[0][col];

		if (!pos.equals("___"))
			return false;
		else 
		{

			String simboloAtacante=jugadores.get(turno).darSimbolo();
			for (int i =  tablero.length-1; i >= 0; i--) {
				String pos2=tablero[i][col];
				if (pos2.equals("___"))
				{
					tablero[i][col]="_"+simboloAtacante+"_";
					
					if(turno==jugadores.size()-1)
						turno=0;
					else
						turno++;
					atacante = jugadores.get(turno).darNombre();
					if(terminar(i, col))
					{
						finJuego=true;
					}
					return true;
				}

			}
			return true;

		}



	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		//TODO
		// col
		String jugada= tablero[fil][col];
		int contador=0;
		int len=tablero[col].length;
		for (int i = 0; i+fil < tablero.length; i++) {
			String pos=tablero[fil+i][col];
			if ( pos.equals(jugada))
				contador++;
			else
				contador=0;
			if(contador>ganar-1)
				return true;

		}
		//fil

		contador=0;
		for (int i = 0; i < tablero[0].length; i++) {
			String pos=tablero[fil][i];
			if ( pos.equals(jugada))
				contador++;
			else 
				contador=0;
			if(contador>ganar-1)
				return true;

		}
		//diag
		//diagonal derecha izquierda despues
		contador=1;
		for(int i=1;i+fil<tablero.length && i+col<tablero[0].length;i++)
		{
			String siguiente=tablero[fil+i][col+i];
			if(siguiente.equals(jugada))
				contador++;
			else
				break;

		}
		if(contador>ganar-1)
			return true;
		//diagonal derecha izquierda antes
		contador=1;
		for(int i=1;fil-i>=0 && col-i>=0;i++)
		{
			String anterior=tablero[fil-i][col-i];
			if(anterior.equals(jugada))
				contador++;
			else
				break;

		}
		if(contador>ganar-1)
			return true;

		//diagonal izquierda derecha antes
		contador=1;
		for(int i=1;i+fil<tablero.length && col-i>=0;i++)
		{
			String siguiente=tablero[fil+i][col-i];
			String psiguiente=siguiente;
			if(siguiente.equals(jugada))
				contador++;
			else
				break;

		}
		if(contador>ganar-1)
			return true;
		//diagonal izquierdad derecha despues1
		
		contador=1;
		for(int i=1;fil-i>=0 && i+col<tablero[0].length;i++)
		{
			String anterior=tablero[fil-i][col+i];
			if(anterior.equals(jugada))
				contador++;
			else
				break;

		}
		if(contador>ganar-1)
			return true;

		return false;

	}
}

