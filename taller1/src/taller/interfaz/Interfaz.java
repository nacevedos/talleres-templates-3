package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		//TODO
		System.out.println("¿Cuantos jugadores ?");
		int cuantos=Integer.parseInt(sc.next());
		ArrayList jugadores= new ArrayList<Jugador>();
		for(int i=0; i<cuantos;i++)
		{
			System.out.println("Nombre del jugador"+(i+1));
			String nombre=sc.next();
			System.out.println("simbolo de jugador (solo un caracter):"+(i+1));
			String simbolo=sc.next();
			Jugador nuevo=new Jugador(nombre, simbolo);
			jugadores.add(nuevo);
		}

		System.out.println("Ingrese el número de filas:");
		String filas =sc.next();
		System.out.println("Ingrese el número de columnas:");
		String columnas =sc.next();
		System.out.println("Ingrese el número de fichas para ganar:");
		String ganar =sc.next();
		int f=Integer.parseInt(filas);
		int c= Integer.parseInt(columnas);
		if(f<4)
			f=4;
		if(c<4)
			c=4;
		juego= new LineaCuatro(jugadores, f, c,Integer.parseInt(ganar));
		imprimirTablero();
		juego();
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		//TODO
		String ganador="";
		while( !juego.fin())
		{
			String nombre=juego.darAtacante();
			System.out.println("turno de " + nombre );
			System.out.println("Marque el número de la columna donde desea jugar");
			String jugada =sc.next();
			ganador=juego.darAtacante();
			juego.registrarJugada(Integer.parseInt(jugada)-1);
			imprimirTablero();
		}


		System.out.println("juego terminado, gana: "+ ganador);

	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
		System.out.println("Nombre del jugador 1:");
		String nombre=sc.next();
		System.out.println("simbolo de jugador 1 (solo un caracter):");
		String simbolo=sc.next();
		System.out.println("Ingrese el número de filas:");
		String filas =sc.next();
		System.out.println("Ingrese el número de columnas:");
		String columnas =sc.next();
		System.out.println("Ingrese el número de fichas para ganar:");
		String ganar =sc.next();
		Jugador jugador1= new Jugador(nombre, simbolo);
		ArrayList jugadores= new ArrayList<Jugador>();
		jugadores.add(jugador1);
		Jugador maquina= new Jugador("maquina", "m");
		jugadores.add(maquina);
		int f=Integer.parseInt(filas);
		int c= Integer.parseInt(columnas);
		juego= new LineaCuatro(jugadores, f, c,Integer.parseInt(ganar));
		imprimirTablero();
		juegoMaquina();
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		//TODO
		String ganador="";
		while( !juego.fin())
		{
			String nombre=juego.darAtacante();
			System.out.println("turno de " + nombre );
			System.out.println("Marque el número de la columna donde desea jugar");
			String jugada =sc.next();
			ganador=juego.darAtacante();
			juego.registrarJugada(Integer.parseInt(jugada)-1);
			if(!juego.fin())
				ganador=juego.darAtacante();
			juego.registrarJugadaAleatoria();
			imprimirTablero();
		}

		System.out.println("juego terminado, gana: "+ ganador);
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		//TODO
		String[][] tablero= juego.darTablero();
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				System.out.print(tablero[i][j]);
				System.out.print(" ");
			}
			System.out.println("");
		}

	}
}
